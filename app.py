from flask import Flask, request, jsonify, render_template, url_for
from redis import Redis
import numbers
import random
from networkx.readwrite import json_graph
import networkx as nx

app = Flask(__name__)
redis = Redis(host='redis', port=6379)


@app.route('/d3')
def show_entries():
    g = nx.gnm_random_graph(random.randrange(100),random.randrange(100))
    graph = json_graph.node_link_data(g)
    return render_template('index.html', graph=graph)


@app.route("/api/hello/<name>", methods=['GET'])
def hello_personalized(name):
    return "Hello World! " + name


@app.route("/api/data", methods=['GET'])
def data():
    my_dict = {
               "temp": [20, 21, 21],
               "time": [10, 20, 30],
               "unit": "s"
              }
    return jsonify(my_dict)


@app.route("/api/add", methods=['POST'])
def add():
    if (request.is_json):
        content = request.get_json()
        if 'a' in content and 'b' in content:
            if isinstance(content['a'], numbers.Number) and isinstance(content['b'], numbers.Number):
                return str(content['a'] + content['b']), 200
    return {}, 400


@app.route('/')
def hello():
    count = redis.incr('hits')
    return 'Hello! I have been seen {} times.\n'.format(count)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)

